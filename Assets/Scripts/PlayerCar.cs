﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCar : MonoBehaviour {

    public float CarHorizontalSpeed = 2f;
    private Vector3 carPosition;

    public float maxDurability = 100f; //wytrzymalosc samochodu
    //[HideInInspector] //po przejsciu do obiektu jest ukryta i nie mozna zmienic jej wartosci
    public float Durability;

    void Start()
    {
        carPosition = gameObject.transform.position;
        Durability = maxDurability;
    }

    void Update()
    {
        carPosition.x += Input.GetAxis("Horizontal") * CarHorizontalSpeed * Time.deltaTime;

        carPosition.x = Mathf.Clamp(carPosition.x, -2.41f, 2.41f);
        gameObject.transform.position = carPosition;
    }
}
