﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonuses : MonoBehaviour
{
    [Header("Type of bonus")]
    public bool isDurability;//typ bool czyli 0/1 true false
    public bool isShield;
    public bool isSpeed;

    [Header("Bonuses Settings")]
    public float bonusSpeed = 10f;

    [Header("Durability Settings")]
    public float repairPoints;

    [Header("Shield Settingd")]
    public GameObject shield;
    private GameObject playerCar;
    private Vector3 playerCarPos;

    [Header("Speed Settingd")]
    public float speedBoost;
    public float duration;
    private bool isActivated = false;

    private void Update()//aby bonusy spadały na planszy
    {
        this.gameObject.transform.Translate(new Vector3(0, -1, 0) * 10 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D colision)//aktywowanie bonusu po kolizji z playerem
    {
        if (colision.gameObject.tag == "Player" || colision.gameObject.tag == "Shield")
        {
            if (isDurability == true)
            {
                colision.gameObject.GetComponent<PlayerCar>().Durability += repairPoints;
                Destroy(this.gameObject);
            }
            else if (isShield == true)
            {
                playerCar = GameObject.FindWithTag("Player");//zmiana tagu z player na shield (ochrona)
                colision.gameObject.tag = "Shield";
                playerCarPos = playerCar.transform.position;
                playerCarPos.z = -0.1f;
                GameObject shieldObj = Instantiate(shield, playerCarPos, Quaternion.identity);//tworzenie tarczy w miejscu samochodu
                shieldObj.transform.parent = playerCar.transform;//zmiana rodzica (poruszanie sie za playerem)
                Destroy(this.gameObject);
            }
            else if (isSpeed == true)
            {
                //gameObject.GetComponent<SpriteRenderer>().enabled = false; //niewidzialnosc po wiechaniu dla bonusu
                isActivated = true;
                StartCoroutine("SpeedBoostActivated");
            }
        }
        else if (colision.gameObject.tag == "EndoftheRoad" && isActivated == false) //jesli wjehalismy w nasz koncowy boxcolider i nasz bonus nie został 
        {                                                                   //aktywowany to zniszcz ten obiekt
            Destroy(this.gameObject);
        }
    }

    IEnumerator SpeedBoostActivated() // do courutyny
    {
        while (duration > 0)
        {
            duration -= Time.deltaTime / speedBoost;
            Time.timeScale = speedBoost; // powyzej 1 czas spowolniony ponizej przyspieszony
            yield return null;
        }
        Time.timeScale = 1f;
        Destroy(this.gameObject);
    }
}
