﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarDurabilityManager : MonoBehaviour {

    public GameObject playerCarPrefab;
    public GameObject spawnPoint;
    public TextMesh durabilityText;
    public int lifes;
    private GameObject playerCar;

    void Start()
    {
        playerCar = (GameObject)Instantiate(playerCarPrefab, spawnPoint.transform.position, Quaternion.identity);
    }

    void Update()
    {
        if(playerCar.GetComponent<PlayerCar>().Durability <= 0)
        {
            Destroy(playerCar);
            lifes--;
            if(lifes > 0)
            {   
                StartCoroutine("SpawnaCar");
            }
        } else if (playerCar.GetComponent<PlayerCar>().Durability > playerCar.GetComponent<PlayerCar>().maxDurability)
        {
            playerCar.GetComponent<PlayerCar>().Durability = playerCar.GetComponent<PlayerCar>().maxDurability;
        }

        durabilityText.text = "Durability: " + playerCar.GetComponent<PlayerCar>().Durability + "/" + playerCar.GetComponent<PlayerCar>().maxDurability;

    }

    IEnumerator SpawnaCar() // do powyższej korutyny
    {
        playerCar = (GameObject)Instantiate(playerCarPrefab, spawnPoint.transform.position, Quaternion.identity);
        playerCar.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.4f); //zmieniamy przezroczystosc auta (kanał alfa)
        playerCar.GetComponent<BoxCollider2D>().isTrigger = true;
        playerCar.tag = "Untouchable";       //samochód niezniszczalny
        yield return new WaitForSeconds(3);  // przez 3 sekundy
        playerCar.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        playerCar.GetComponent<BoxCollider2D>().isTrigger = false;
        playerCar.tag = "Player";
    }

}
