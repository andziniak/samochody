﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivilCarSpawner : MonoBehaviour {

    public float carSpawnDelay = 2f; //co 2 sekundy nowy spawn
    public GameObject civilCar;

    private float spawnDelay;

    private float[] lanesArray; //prywatna tablica pasów
    
    void Start()
    {
        lanesArray = new float[4];
        lanesArray[0] = -2.11f;
        lanesArray[1] = -0.76f;
        lanesArray[2] =  0.76f;
        lanesArray[3] =  2.11f;
        spawnDelay = carSpawnDelay;
    }

    void Update()
    {
        spawnDelay -= Time.deltaTime;
        if (spawnDelay <= 0)
        {
            spawnCar();
            spawnDelay = carSpawnDelay;
        }
    }

    void spawnCar()
    {
        int lane = Random.Range(0, 4); // zmienna lane wybierana losowo
        if(lane==0 || lane==1)
        {
            GameObject car = (GameObject)Instantiate(civilCar, new Vector3(lanesArray[lane], 6f, 0), Quaternion.Euler(new Vector3(0,0,180)));
            car.GetComponent<CivilCarBehaviour>().direction = 1;
            car.GetComponent<CivilCarBehaviour>().civilCarSpeed = 10f;
        }

        if (lane == 2 || lane == 3)
        {
            Instantiate(civilCar, new Vector3(lanesArray[lane], 6f, 0), Quaternion.identity);
        }
    }
}
