﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour {

    private float delay;
    public GameObject[] bonuses;
    public int minDelay;
    public int maxDelay;

    private void Start()
    {
        delay = Random.Range(minDelay, maxDelay);//losowo wybiera wartosc z przedzialu
    }

    private void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0)
        {
            delay = Random.Range(minDelay, maxDelay);
            SpawnBonus();//odpal funkcje spawnbonus
        }
    }

    void SpawnBonus()
    {
        Instantiate(bonuses[Random.Range(0,3)], new Vector3(Random.Range(-2.4f, 2.4f), 6f, 0), Quaternion.identity);
    }
}
