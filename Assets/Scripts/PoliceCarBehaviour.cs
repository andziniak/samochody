﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceCarBehaviour : MonoBehaviour {


    public Light RedLight;
    public Light BlueLight;
    public float lightDelay;
    private float lightShowDelay;
    public GameObject bullet;
    public float shootingSeriesDelay;
    public float singleShotDelay;
    
    public bool isLeft;
    public float policeCarVerticalSpeed;
    private float shootDelay;
    private Vector3 policeCarPos;
    private GameObject bulletObj;
    public int bulletInSeries;


    private void Start()
    {
        shootDelay = shootingSeriesDelay;

        lightShowDelay = 2 * lightDelay; // do swiatel radiowozu
    }

    private void Update()
    {
        lightShowDelay -= Time.deltaTime;
        if(lightShowDelay>lightDelay)
        {
            BlueLight.enabled = false;
            RedLight.enabled = true;
        }
        else if(lightShowDelay <= lightDelay && lightShowDelay >0)
        {
            BlueLight.enabled = true;
            RedLight.enabled = false;
        }
        else if(lightShowDelay <=0)
        {
            lightShowDelay = 2 * lightDelay;
        }

        if (gameObject.transform.position.y < -3.8f)
        {
            gameObject.transform.Translate(new Vector3(0, 1, 0) * policeCarVerticalSpeed * Time.deltaTime);
        }
        else
        {
            shootDelay -= Time.deltaTime;
            if (shootDelay <= 0)
            {
                StartCoroutine("Shoot");
                shootDelay = shootingSeriesDelay;
            }
        }
    }
    IEnumerator Shoot()
    {
         for (int i = bulletInSeries; i>0; i--)
        {
            bulletObj = (GameObject) Instantiate(bullet, transform.position, Quaternion.identity);
            if (isLeft == true)
            {
                bulletObj.GetComponent<Bullet>().direction = 1; 
            }
            else if (isLeft == false)
            {
                bulletObj.GetComponent<Bullet>().direction = -1;
            }
            yield return new WaitForSeconds(singleShotDelay);//na koniec każdej pentli odczekamy wartosc zmiennej
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Barrier")
        {
            Destroy(this.gameObject);
        }
    }
}

