﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour {

    public float scrollspeed;
    private Vector2 offset;

    void Update()
    {
        offset = new Vector2(0, Time.time * scrollspeed);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
