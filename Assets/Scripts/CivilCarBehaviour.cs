﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivilCarBehaviour : MonoBehaviour {

    public float crashDamage = 20f;

    public float civilCarSpeed = 5f;
    public int direction = -1;

    private Vector3 civilCarPosition;

    private void Update()
    {
        gameObject.transform.Translate(new Vector3(0, direction, 0) * civilCarSpeed * Time.deltaTime);  
    }

    private void OnCollisionEnter2D(Collision2D collision) //kolizja z bokiem auta (nie zważa na uruchomiony trigger)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerCar>().Durability -= crashDamage / 5;
        }
    }


    private void OnTriggerEnter2D(Collider2D colision)
    {
        if(colision.gameObject.tag == "Player")
        {
            colision.gameObject.GetComponent<PlayerCar>().Durability -= crashDamage;
            Debug.Log("Gracz w nas wjechał");
            Destroy(this.gameObject);
        }
        else if(colision.gameObject.tag == "EndoftheRoad")
        {
            Destroy(this.gameObject);
        }
    }
}
