﻿using UnityEngine;
using System.Collections;

public class PlayerCarMovement : MonoBehaviour {

    public float carHorizontalSpeed = 2f;
    private Vector3 carPosition;

    public float maxDurability = 100f;
    
    public float durability = 1f;


    void Start()
    {
        carPosition = this.gameObject.transform.position;
        durability = maxDurability;
    }

    void Update()
    {
        carPosition.x += Input.GetAxis("Horizontal") * carHorizontalSpeed * Time.deltaTime;
        carPosition.x = Mathf.Clamp(carPosition.x, -2.41f, 2.41f);
        this.gameObject.transform.position = carPosition;
    }

}
